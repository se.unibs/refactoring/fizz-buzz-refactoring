package it.unibs.ing.se.refactoring.fizzbuzz;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class FizzBuzzTest {
	@ParameterizedTest
	@CsvSource({
			"1,1", "2,2", "3,fizz", "4,4", "5,buzz", "15,fizzbuzz", "6,fizz", "20,buzz", "75,fizzbuzz",
//			"7,crash", "42,fizzcrash", "35,buzzcrash", "105,fizzbuzzcrash", "210,fizzbuzzcrash"
	})
	public void doTest(int input, String expected) {
		assertThat(FizzBuzz.play(input), is(equalTo(expected)));
	}
}
